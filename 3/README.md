# Úloha 3

## 1. úloha - Dockerfile

Ako webovú aplikáciu som použil domácu úlohu z NI-AM2, ktorá má jeden jednoduchý endpoint. Na endpointe `/{meno}` vraciam testovaciu hlášku Hello {meno}.

```dockerfile
# Specify the base image
FROM node:14

# Set the working directory inside the container
WORKDIR /app

# Copy the server implementation to the working directory
COPY . .

# Install dependencies
RUN npm install

# Expose port 8888
EXPOSE 8888

# Start the server
CMD ["npm", "start"]
```

Následne som docker zbuildil, spustil a otestoval jeho funkčnosť.


![](1build.png)
![](1run.png)
![](1test.png)

## 2. úloha - Analýza obrazu

Použil som `docker save`, a `docker history` na získanie informácií o obraze a jeho vrstvách, prípadne - ak som si nebol istý - som danú vrstvu rozbalil.

```
docker save vcc/webapp -o webapp.tar
```
![](2layers.png)

![](2history.png)


Z týchto informácii som zistil že prvných 5 vrstiev zhora odpovedá mnou definovaným príkazom v Dockerfile. Ostatné podkladovému obrazu node:14

## 3. úloha - Vytvorenie systémového kontajneru pomocou `systemd-nspawn`

Postupoval som podľa návodu z cvičenia na https://wiki.archlinux.org/title/Systemd-nspawn[Arch wiki^].

Do `/etc/dnf/dnf.conf` som pridal repozitár Fedora:
```
[fedora]
name=Fedora $releasever - $basearch
metalink=https://mirrors.fedoraproject.org/metalink?repo=fedora-$releasever&arch=$basearch
gpgkey=https://getfedora.org/static/fedora.gpg
```

Pripojil som sa k rootu, vytvoril kontajner, zmenil defaultné heslo a spustil:

```
dnf --releasever=37 --best --setopt=install_weak_deps=False --repo=fedora --installroot=/home/fedora --nodocs install systemd passwd dnf fedora-release
systemd-nspawn -D /home/fedora passwd
systemd-nspawn -b -D /home/fedora
```

![](3run.png)

## 4. úloha - bezpečný Systemd file

Opäť som využil tretiu úlohu z NI-AM2.

Najprv som si vytvoril súbor.

```shell
nano /etc/systemd/system/webapp.service
```

Do ktorého som napísal:

```text
[Unit]
Description=Webapp for NI-AM2

[Service]
ExecStart=/usr/bin/node /home/webapp/main.js

[Install]
WantedBy=default.target
```

Potom som spustil analýzu.

```shell
systemd-analyze security webapp
```

Výsledné skóre bolo **9.6**.

Potom som do /etc/systemd/system/webapp.service začal pridávať nové direktívy podľa [návodu](https://www.opensourcerers.org/2022/04/25/optimizing-a-systemd-service-for-security/)

Pre istotu som reštartoval kontajner a následne zas spustil analýzu, podľa ktorej sa appka dostala na lepšie ako žlté skóre **6.0**.

![](4directives.png)

```shell
systemctl stop webapp.service
systemctl start webapp.service
systemd-analyze security webapp
```

![](4score.png)