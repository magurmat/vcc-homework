Najprv som postupoval podla navodu z 3. cvicenia.

Potom som si do compute uzlov pridal obe nody miesto iba jedneho.

A vytvoril som si dve instancie cez prikazy 

> openstack server create --flavor m1.tiny --image cirros --key-name mykey --availability-zone nova:magurmat-01 --network demo-net demo1

> openstack server create --flavor m1.tiny --image cirros --key-name mykey --availability-zone nova:magurmat-02 --network demo-net demo2

![](1dashboard.png)


Pingovanie medzi VM

![](3pingcontainer.png)
![](4pingcontainer.png)

Nastavenie pripojenia do internetu pomocou

> echo 1 > /proc/sys/net/ipv4/ip_forward

> ip addr add 10.0.2.1/24 brd + dev veth1

> iptables -t nat -A POSTROUTING -s 10.0.2.0/24 -j MASQUERADE

![](5pinginternet.png)
![](6pinginternet.png)
![](7topology.png)