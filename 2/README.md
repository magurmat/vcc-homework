# Druhá úloha

## Objavenie XML deskriptoru


```bash
root@magurmat-01:~ docker exec -it nova_libvirt bash -i
```

![](1list.png)

![](1interface.png)

odtiaľ som získal názov tapdc6c5075-a1, ktorý som použil ďalej.



## Trasovanie trafficu medzi 2 VM

Ďalej som pustil ping z demo02 na demo01

![](1ping.png)

a na magurmat-01.vcc som sledoval traffic

![](1result.png)

## Trasovanie vo Wiresharku


Vytvoril som si dump, ten som si preniesol k sebe na PC a lokálne si ho otvoril vo Wiresharku

![](2ensdump.png)

![](2wireshark.png)

